// ==UserScript==
// @name        Issue Helper
// @namespace   http://drupal.org/project/issue_helper
// @version     1.0
// @description Helps you to administer drupal.org issues more efficiently.
// @author      Karsten Frohwein
// @include     http://drupal.org/node/*
// @require     http://code.jquery.com/jquery-latest.min.js
// @require     http://timeago.yarp.com/jquery.timeago.js
// @resource    IHPostpone Issue_Helper.postpone_message.html
// @resource    IHSupport Issue_Helper.support_message.html
// @resource    IHStyles Issue_Helper.css
// ==/UserScript==
(function () {
  "use strict";

  // We only work on not "fixed" issues.
  var ih_status = $('#edit-sid').val();

  if (ih_status !== '1' && ih_status !== '8' && ih_status !== '13') {
    return;
  }

  GM_addStyle(GM_getResourceText('IHStyles'));

  var time_string = $('.submitted:last em').html();
  var time_parts = time_string.match(/(.*) at (\d+):(\d+)(am|pm)/);
  var time_obj = new Date(time_parts[1]);
  if (time_parts[4] === 'pm') {
    time_parts[2] = parseInt(time_parts[2], 10) + 12;
  }
  time_obj.setHours(time_parts[2]);
  time_obj.setMinutes(time_parts[3]);
  var time_ago = $.timeago(time_obj);


  $('#comment-form').prepend($('<fieldset id="issue-helper"><legend>Issue Helper</legend><div>Last edit was ' + time_ago + '</div></fieldset>'));

  var postpone_button = $('<div class="ih-button" id="ih-postpone">Postpone and ask for more information</div>');
  postpone_button.click(function () {
    $('#edit-project-info-assigned').val(0);
    $('#edit-priority').val(2);
    $('#edit-sid').val(16);
    $('#edit-comment').val(GM_getResourceText('IHPostpone'));
    $('#edit-taxonomy-tags-9').val('Needs steps to reproduce');
    $('#edit-submit').click();
  });

  $('#issue-helper').append(postpone_button);


  if ($('#edit-category').val() === 'support') {
    var support_button = $('<div class="ih-button" id="ih-support">Set support Issue to "fixed"</div>');
    support_button.click(function () {
      $('#edit-project-info-assigned').val(0);
      $('#edit-priority').val(2);
      $('#edit-sid').val(5);
      $('#edit-comment').val(GM_getResourceText('IHSupport'));
      $('#edit-submit').click();
    });

    $('#issue-helper').append(support_button);
  }
}());